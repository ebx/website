/* Set the width of the sidebar to 250px (show it) */
function openNav() {
  document.getElementById("mySidepanel").style.width = "250px";
}

/* Set the width of the sidebar to 0 (hide it) */
function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
}

function dispAlbum() {
  const divToDisp = document.getElementById("displayAlbumDiv");
  let randomNumber = Math.trunc(Math.random() * 50);
  while (randomNumber === 0) {
    randomNumber = Math.trunc(Math.random() * 50);
  }
  const albums = ["", "The Beach Boys - Pet Sounds",
  "Neutral Milk Hotel - In the Aeroplane Over the Sea",
  "Luiz Bonfá & Maria Toledo - Braziliana",
  "Steve Hiett - Down on the Road by the Beach",
  "Tito Puente - Dance Mania, Volume 1",
  "Antonio Carlos Jobim - Wave",
  "WHY? - Alopecia",
  "Miracle Musical - Hawaii: Part II",
  "Vampire Weekend - Vampire Weekend",
  "Steely Dan - Can't Buy a Thrill",
  "Bruno Pernadas - Those Who Throw Objects At the Crocodiles Will Be Asked To Retrieve Them",
  "Fang Island - Fang Island",
  "Buena Vista Social Club - Buena Vista Social Club",
  "Animal Collective - Strawberry Jam",
  "Mild High Club - Skiptracing",
  "The Beach Boys - The Smile Sessions",
  "The Avalanches - Since I Left You",
  "Weyes Blood - Titanic Rising",
  "Various Artists - Katamari Fortissimo Damacy",
  "WHY? - Almost Live from Eli's Live Room",
  "Vince Guaraldi Trio - A Charlie Brown Christmas",
  "Candy Claws - Ceres & Calypso in the Deep Time",
  "The Umpteens - Turn the Livingroom Into a Dance Floor",
  "Anri - TIMELY!!",
  "Sergio Mendes & Brasil '66 - Herb Alpert Presents Sergio Mendes & Brasil '66",
  "Various Artists - La La Land: The Complete Musical Experience",
  "Daft Punk - Random Access Memories",
  "Against All Logic - 2012-2017",
  "The KLF - Chill Out",
  "Jai Paul - Jai Paul",
  "Neil Cicierega - Mouth Sounds",
  "DJ Sabrina the Teenage DJ - Makin' Magic",
  "John Guscott - de Blob",
  "Azteca - Azteca",
  "Kero Kero Bonito - Bonito Generation",
  "Temporex - Care",
  "Tame Impala - Lonerism",
  "Herbert - Scale",
  "Magdalena Bay - Mercurial World",
  "Roar - I Can't Handle Change",
  "Car Seat Headrest - Twin Fantasy",
  "Sleigh Bells - Treats",
  "Neil Cicierega - Mouth Moods",
  "BoAT - RORO",
  "The Beatles - Abbey Road",
  "Animal Collective - Merriweather Post Pavilion",
  "Black Country, New Road - Ants From Up There",
  "Bo En - Pale Machine",
  "Hot Hot Heat - Elevator",
  "Quasi - R&B Transmogrification"];
  const imgTag = `<img class="img-thumbnail" src="static/albums/${randomNumber}.jpg" width="350" height="350">`;
  const albumInfoArr = albums[randomNumber].split(" - ").reverse();
  const albumInfo = `<h3>${albumInfoArr[0]}</h3><h5>${albumInfoArr[1]}</h5>`;
  divToDisp.innerHTML = imgTag + albumInfo;
}

const images = document.querySelectorAll(".carousel-imgs > img");
let shownIdx = 0;
images.forEach((elem, idx) => {
  if (idx !== shownIdx) {
    elem.classList.add("hide");
  }
});
const prevBtn = document.querySelector(".prev-btn");
const nextBtn = document.querySelector(".next-btn");

function toggleElems(elem, shownIdx, len, idx) {
  if (shownIdx % len === idx) {
    elem.classList.remove("hide");
    elem.classList.add("show");
  } else {
    elem.classList.remove("show");
    elem.classList.add("hide");
  }
}

nextBtn.addEventListener("click", () => {
  shownIdx++;
  images.forEach((elem, idx) => {
    toggleElems(elem, shownIdx, images.length, idx);
  });
});

prevBtn.addEventListener("click", () => {
  shownIdx--;
  images.forEach((elem, idx) => { 
    if (shownIdx < 0) {
      shownIdx = images.length - 1;
    }
    toggleElems(elem, shownIdx, images.length, idx);
  });
});